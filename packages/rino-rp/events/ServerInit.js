module.exports = {
    "ServerInit": () => {
        console.log("Запуск сервера...");
        initMpUtils();
        initPoliceCells();
        initAtm();

        require('../modules/economy.js').Init(async () => {
            mp.updateWorldTime();
            require('../modules/mailSender.js').Init();
            require('../modules/houses.js').Init();
            require('../modules/interiors.js').Init();
            require('../modules/garages.js').Init();
            require('../modules/timers.js').InitPayDay();
            require('../modules/bizes.js').Init();
            require('../modules/inventory.js').Init();
            require(`../modules/achievements.js`).Init();
            require('../modules/factions.js').Init();
            require('../modules/vehicles.js').Init();
            //require('../modules/objects.js').Init(); //wait sync fix
            // require('../modules/routep.js').Init();
            require('../modules/jobs/trash/trash.js').Init();
            require('../modules/jobs/gopostal/gopostal.js');
            require('../modules/jobs/pizza/index.js');
            require('../modules/jobs/waterfront/index.js');
            require('../modules/jobs/builder/index.js');
            require('../modules/jobs/autoroober/index.js');
            require('../modules/jobs/taxi/index.js');
            require('../modules/jobs/smuggling/index.js');
            require('../modules/clothes.js').Init();
            require('../modules/ls_customs/index.js');
            require('../modules/rent_veh.js');
            require('../modules/logs.js').Init();
            require('../modules/reports.js').Init();
            require('../modules/v2_reports.js').Init();
            require('../modules/parkings.js').Init();
            require('../modules/donate.js').Init();
            require('../modules/autosaloons.js').Init();
            require('../modules/bank.js');
            require(`../modules/jobs.js`).Init();
            require(`../modules/driving_school.js`).Init();
            require(`../modules/jobs/trucker/trucker.js`).Init();
            require('../modules/barbershop/index.js');
            require(`../modules/markers.js`).Init();
            require(`../modules/peds.js`).Init();
            require(`../modules/farms.js`).Init();
            require('../modules/doorControl.js');
            require('../modules/cutscenes.js').Init();
            require('../modules/spawn.js').Init();
            require('../modules/ipl.js').Init();
            require('../modules/whitelist.js').Init();
            await require('../modules/green_zones.js').Init();
            require('../modules/phone.js').Init();
            require('../modules/vehicleSeller.js');
        });

        require('../modules/forefinger.js');
        console.log("Сервер запущен!");
    }
}

function initMpUtils() {
    mp.randomInteger = (min, max) => {
        var rand = min - 0.5 + Math.random() * (max - min + 1)
        rand = Math.round(rand);
        return rand;
    }

    mp.players.getBySqlId = (sqlId) => {
        if (!sqlId) return null;
        var result;
        mp.players.forEach((recipient) => {
            if (recipient.sqlId == sqlId) {
                result = recipient;
                return;
            }
        });
        return result;
    }

    mp.players.getByLogin = (login) => {
        if (!login) return null;
        var result;
        mp.players.forEach((recipient) => {
            if (recipient.account.login == login) {
                result = recipient;
                return;
            }
        });
        return result;
    }

    mp.players.getBySocialClub = (socialClub) => {
        if (!socialClub) return null;
        var result;
        mp.players.forEach((recipient) => {
            if (recipient.socialClub == socialClub) {
                result = recipient;
                return;
            }
        });
        return result;
    }

    mp.players.getByName = (name) => {
        if (!name) return null;
        var result;
        mp.players.forEach((recipient) => {
            if (recipient.name == name) {
                result = recipient;
                return;
            }
        });
        return result;
    }

    mp.vehicles.getBySqlId = (sqlId) => {
        if (!sqlId) return null;
        var result;
        mp.vehicles.forEach((veh) => {
            if (veh.sqlId == sqlId) {
                result = veh;
                return;
            }
        });
        return result;
    }

    /* Полное удаление предметов инвентаря с сервера. */
    mp.fullDeleteItemsByParams = (itemId, keys, values) => {
        //debug(`fullDeleteItemsByParams: ${itemId} ${keys} ${values}`);
        /* Для всех игроков. */
        mp.players.forEach((rec) => {
            if (rec.sqlId) rec.inventory.deleteByParams(itemId, keys, values);
        });
        /* Для всех объектов на полу. */
        mp.objects.forEach((obj) => {
            if (obj.getVariable("inventoryItemSqlId") > 0) {
                var item = obj.item;
                var doDelete = true;
                for (var i = 0; i < keys.length; i++) {
                    var param = item.params[keys[i]];
                    if (!param) {
                        doDelete = false;
                        break;
                    }
                    if (param && param != values[i]) {
                        doDelete = false;
                        break;
                    }
                }
                if (doDelete) {
                    DB.Handle.query(`DELETE FROM inventory_players WHERE id=?`, obj.getVariable("inventoryItemSqlId"));
                    obj.destroy();
                }
            }
        });
        /* Для всех игроков из БД. */
        // TODO: ^^
    }

    mp.fullDeleteItemsByFaction = (playerSqlId, factionId) => {
        // debug(`fullDeleteItemsByFaction: ${playerSqlId} ${factionId}`);
        var items = {
            "2": [1, 2, 3, 6, 7, 8, 9, 10, 14, 17, 18, 19, 20, 21, 22, 23, 27, 29], // LSPD
            "3": [1, 2, 3, 6, 7, 8, 9, 10, 14, 17, 18, 19, 20, 21, 22, 23, 27, 29], // BCSO
            "4": [1, 2, 3, 6, 7, 8, 9, 10, 14, 17, 18, 19, 20, 21, 22, 23, 29, 61], // FIB
            "5": [1, 2, 3, 6, 7, 8, 9, 10, 14, 24, 25, 27, 63], // EMC
            "6": [1, 2, 3, 6, 7, 8, 9, 10, 14, 20, 21, 22, 23, 27, 60], // Fort Zancudo
            "7": [1, 2, 3, 6, 7, 8, 9, 10, 14, 20, 21, 22, 23, 27, 60], // Air Army
        };
        if (items[factionId]) {
            items[factionId].forEach((itemId) => {
                mp.fullDeleteItemsByParams(itemId, ["owner", "faction"], [playerSqlId, factionId]);
            });
        }
    }

    mp.getNearVehicle = (pos, range) => {
        var nearVehicle;
        var minDist = 99999;
        mp.vehicles.forEachInRange(pos, range, (veh) => {
            var distance = veh.dist(pos);
            if (distance < minDist) {
                nearVehicle = veh;
                minDist = distance;
            }
        });
        return nearVehicle;
    }

    mp.setVehSpawnTimer = (vehicle) => {
        var havePlayers = vehicle.getOccupants().length > 0;
        if (!havePlayers) vehicle.utils.setSpawnTimer(mp.economy["car_spawn_time"].value);
    }

    mp.updateWorldTime = () => {
        var speed = mp.economy["world_time_speed"].value;
        var worldHour = new Date().getHours() * speed % 24;
        worldHour += parseInt(new Date().getMinutes() / (60 / speed));
        if (worldHour != mp.world.time.hour) {
            mp.world.time.hour = worldHour;
            //debug(`Игровое время обновлено: ${mp.world.time.hour} ч. `);
        }
    }


    /* Оповещаем членов организации о том, что вошел коллега. */
    mp.broadcastEnterFactionPlayers = (player) => {
        if (!player.faction) return;
        var rankName = mp.factions.getRankName(player.faction, player.rank);
        if (mp.factions.isHospitalFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    if (rec.sqlId != player.sqlId) rec.call(`tablet.medic.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: rankName
                    }]);
                    player.call(`tablet.medic.addTeamPlayer`, [{
                        id: rec.id,
                        name: rec.name,
                        rank: mp.factions.getRankName(rec.faction, rec.rank)
                    }]);
                }
            });
        } else if (player.faction == 2) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    if (rec.sqlId != player.sqlId) rec.call(`tablet.police.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: rankName
                    }]);
                    player.call(`tablet.police.addTeamPlayer`, [{
                        id: rec.id,
                        name: rec.name,
                        rank: mp.factions.getRankName(rec.faction, rec.rank)
                    }]);
                }
            });
        } else if (player.faction == 3) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    if (rec.sqlId != player.sqlId) rec.call(`tablet.sheriff.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: rankName
                    }]);
                    player.call(`tablet.sheriff.addTeamPlayer`, [{
                        id: rec.id,
                        name: rec.name,
                        rank: mp.factions.getRankName(rec.faction, rec.rank)
                    }]);
                }
            });
        } else if (player.faction == 4) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    if (rec.sqlId != player.sqlId) rec.call(`tablet.fib.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: rankName
                    }]);
                    player.call(`tablet.fib.addTeamPlayer`, [{
                        id: rec.id,
                        name: rec.name,
                        rank: mp.factions.getRankName(rec.faction, rec.rank)
                    }]);
                }
            });
        } else if (mp.factions.isArmyFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    if (rec.sqlId != player.sqlId) rec.call(`tablet.army.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: rankName
                    }]);
                    player.call(`tablet.army.addTeamPlayer`, [{
                        id: rec.id,
                        name: rec.name,
                        rank: mp.factions.getRankName(rec.faction, rec.rank)
                    }]);
                }
            });
        }
    }

    /* Оповещаем членов организации о том, что вышел коллега. */
    mp.broadcastExitFactionPlayers = (player) => {
        if (!player.faction) return;
        if (mp.factions.isHospitalFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) rec.call(`tablet.medic.removeTeamPlayer`, [player.id]);
            });
        } else if (player.faction == 2) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) rec.call(`tablet.police.removeTeamPlayer`, [player.id]);
            });
        } else if (player.faction == 3) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) rec.call(`tablet.sheriff.removeTeamPlayer`, [player.id]);
            });
        } else if (player.faction == 4) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) rec.call(`tablet.fib.removeTeamPlayer`, [player.id]);
            });
        } else if (mp.factions.isArmyFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) rec.call(`tablet.army.removeTeamPlayer`, [player.id]);
            });
        }
    }

    mp.updateFactionPlayers = (player) => {
        if (!player.faction) return;
        if (mp.factions.isHospitalFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    rec.call(`tablet.medic.removeTeamPlayer`, [player.id]);
                    rec.call(`tablet.medic.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: mp.factions.getRankName(player.faction, player.rank)
                    }]);
                }
            });
        } else if (player.faction == 2) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    rec.call(`tablet.police.removeTeamPlayer`, [player.id]);
                    rec.call(`tablet.police.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: mp.factions.getRankName(player.faction, player.rank)
                    }]);
                }
            });
        } else if (player.faction == 3) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    rec.call(`tablet.sheriff.removeTeamPlayer`, [player.id]);
                    rec.call(`tablet.sheriff.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: mp.factions.getRankName(player.faction, player.rank)
                    }]);
                }
            });
        } else if (player.faction == 4) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    rec.call(`tablet.fib.removeTeamPlayer`, [player.id]);
                    rec.call(`tablet.fib.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: mp.factions.getRankName(player.faction, player.rank)
                    }]);
                }
            });
        } else if (mp.factions.isArmyFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) {
                    rec.call(`tablet.army.removeTeamPlayer`, [player.id]);
                    rec.call(`tablet.army.addTeamPlayer`, [{
                        id: player.id,
                        name: player.name,
                        rank: mp.factions.getRankName(player.faction, player.rank)
                    }]);
                }
            });
        }
    }

    mp.clearFactionPlayers = (player) => {
        if (!player.faction) return;
        if (mp.factions.isHospitalFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) player.call(`tablet.medic.removeTeamPlayer`, [rec.id]);
            });
        } else if (player.faction == 2) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) player.call(`tablet.police.removeTeamPlayer`, [rec.id]);
            });
        } else if (player.faction == 3) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) player.call(`tablet.sheriff.removeTeamPlayer`, [rec.id]);
            });
        } else if (player.faction == 4) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) player.call(`tablet.fib.removeTeamPlayer`, [rec.id]);
            });
        } else if (mp.factions.isArmyFaction(player.faction)) {
            mp.players.forEach((rec) => {
                if (rec.faction == player.faction) player.call(`tablet.army.removeTeamPlayer`, [rec.id]);
            });
        }
    }

    mp.getLicName = (license) => {
        var names = {
            1: "Лицензия на автомобиль",
            2: "Лицензия на мототехнику",
            3: "Лицензия на лодку",
            4: "Лицензия на яхту",
            11: "Лицензия на вертолёт",
            12: "Лицензия на самолёт",
        };
        if (!names[license]) return "Лицензия";
        return names[license];
    }

    mp.convertMinutesToLevelRest = (minutes) => {
        var exp = parseInt(minutes / 60);
        if (exp < 8) return {
            level: 1,
            nextLevel: 8,
            rest: exp
        };
        var i = 2;
        var add = 16;
        var temp = 24;
        while (i < 200) {
            if (exp < temp) {
                return {
                    level: i,
                    nextLevel: temp - add,
                    rest: exp - (temp - add)
                };
            }
            i++;
            add += 8;
            temp += add;
        }
        return -1;
    }

    mp.getPointsOnInterval = (point1, point2, step) => {
        var vectorX = point2.x - point1.x;
        var vectorY = point2.y - point1.y;

        var vectorLenght = Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
        var countOfPoint = parseInt(vectorLenght / step);

        var stepX = vectorX / countOfPoint;
        var stepY = vectorY / countOfPoint;

        var pointsOnInterval = [];

        for (var i = 1; i < countOfPoint; i++) {
            var point = {
                x: point1.x + stepX * i,
                y: point1.y + stepY * i
            }
            pointsOnInterval.push(point);
        }

        return pointsOnInterval;
    }

    mp.broadcastAdmins = (text) => {
        mp.players.forEach((rec) => {
            if (rec.sqlId && rec.admin) rec.call("chat.custom.push", [text]);
            // chatAPI.custom_push(`<a style="color: #FF0000">[A] Tomat Petruchkin:</a> всем доброго времени суток!`);
        });
    }

}

function initPoliceCells() {
    var cellOne = new mp.Vector3(460.03, -994.1, 24.91);
    cellOne.h = 268.34;

    var cellTwo = new mp.Vector3(460.09, -998.03, 24.91);
    cellTwo.h = 268.34;

    var cellThree = new mp.Vector3(460.02, -1001.57, 24.91);
    cellThree.h = 268.34;

    var cellsExit = new mp.Vector3(461.64, -989.16, 24.91);
    cellsExit.h = 93.45;

    mp.policeCells = [cellOne, cellTwo, cellThree];
    mp.policeCellsExit = cellsExit;
}

function initAtm() {
    var atms = [
        '-1109.797, -1690.808, 4.375014',
        '-821.6062, -1081.885, 11.13243',
        '-537.8409, -854.5145, 29.28953',
        '-1315.744, -834.6907, 16.96173',
        '-1314.786, -835.9669, 16.96015',
        '-1570.069, -546.6727, 34.95547',
        '-1571.018, -547.3666, 34.95734',
        '-866.6416, -187.8008, 37.84286',
        '-867.6165, -186.1373, 37.8433',
        '-721.1284, -415.5296, 34.98175',
        '-254.3758, -692.4947, 33.63751',
        '24.37422, -946.0142, 29.35756',
        '130.1186, -1292.669, 29.26953',
        '129.7023, -1291.954, 29.26953',
        '129.2096, -1291.14, 29.26953',
        '288.8256, -1282.364, 29.64128',
        '1077.768, -776.4548, 58.23997',
        '527.2687, -160.7156, 57.08937',
        '-867.5897, -186.1757, 37.84291',
        '-866.6556, -187.7766, 37.84278',
        '-1205.024, -326.2916, 37.83985',
        '-1205.703, -324.7474, 37.85942',
        '-1570.167, -546.7214, 34.95663',
        '-1571.056, -547.3947, 34.95724',
        '-57.64693, -92.66162, 57.77995',
        '527.3583, -160.6381, 57.0933',
        '-165.1658, 234.8314, 94.92194',
        '-165.1503, 232.7887, 94.92194',
        '-2072.445, -317.3048, 13.31597',
        '-3241.082, 997.5428, 12.55044',
        '-1091.462, 2708.637, 18.95291',
        '1172.492, 2702.492, 38.17477',
        '1171.537, 2702.492, 38.17542',
        '1822.637, 3683.131, 34.27678',
        '1686.753, 4815.806, 42.00874',
        '1701.209, 6426.569, 32.76408',
        '-95.54314, 6457.19, 31.46093',
        '-97.23336, 6455.469, 31.46682',
        '-386.7451, 6046.102, 31.50172',
        '-1091.42, 2708.629, 18.95568',
        '5.132, -919.7711, 29.55953',
        '-660.703, -853.971, 24.484',
        '-2293.827, 354.817, 174.602',
        '-2294.637, 356.553, 174.602',
        '-2295.377, 358.241, 174.648',
        '-1409.782, -100.41, 52.387',
        '-1410.279, -98.649, 52.436'
    ];

    atms.forEach((atm) => {
        var pos = atm.split(",");
        var blip = mp.blips.new(500, new mp.Vector3(parseInt(pos[0]), parseInt(pos[1]), parseInt(pos[2])), {
            color: 26,
            name: "ATM",
            shortRange: true,
            scale: 0.9,
            drawDistance: 5,
            dimension: 0
        });
    });
}