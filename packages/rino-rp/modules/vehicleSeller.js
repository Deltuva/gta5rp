// Создаём BLIP
mp.blips.new(633, new mp.Vector3(-241.37, 6254.91, 30.76), {
  name: 'Скупчик авто',
  color: 66,
  scale: 1,
  shortRange: true
});

// Создаём авто
const vehicle_1 = mp.vehicles.new(mp.joaat("phantom"), new mp.Vector3(-238, 6262.2, 32.40), {
  locked: false,
  numberPlate: "KING",
  color: [89, 89]
});
vehicle_1.rotation = new mp.Vector3(0, 0, -90);

const vehicle_2 = mp.vehicles.new(mp.joaat("ardent"), new mp.Vector3(-227.84, 6263.62, 31.49), {
  locked: true,
  numberPlate: "KING",
  color: [89, 89]
});
vehicle_2.rotation = new mp.Vector3(0, 0, 120);

const trailer_1 = mp.vehicles.new(mp.joaat("tr4"), new mp.Vector3(-241.37, 6254.91, 30.76), {
  numberPlate: "ADMIN",
  color: [0, 0]
});
trailer_1.rotation = new mp.Vector3(0, 0, -20);

const trailer_2 = mp.vehicles.new(mp.joaat("tr2"), new mp.Vector3(-220, 6251.82, 31.49), {
  numberPlate: "ADMIN",
  color: [0, 0]
});
trailer_2.rotation = new mp.Vector3(0, 0, -47.5);

let jobTaxiColShape = mp.colshapes.newSphere(-241.37, 6254.91, 30.76, 1.5);

mp.events.call("prompt.show", `Нажмите <span>Е</span> для взаимодействия`);